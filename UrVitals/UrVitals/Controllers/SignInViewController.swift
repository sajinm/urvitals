//
//  SingInViewController.swift
//  UrVitals
//
//  Created by Sajin M on 27/05/2021.
//

import UIKit
import MRCountryPicker
import TextFieldEffects
import Alertift
import FirebaseAuth
import SVProgressHUD
import FirebaseFirestore
import FirebaseFirestoreSwift
import CodableFirebase

class SignInViewController: BaseViewController {
    
    
    @Published var users = [UserModel]()
    
    @IBOutlet weak var txtCode: HoshiTextField!
    @IBOutlet weak var txtPhone: HoshiTextField!
    @IBOutlet weak var txtOtp: HoshiTextField!
    
    @IBOutlet weak var otpView: UIView!
    @IBOutlet weak var registerView: UIView!
    @IBOutlet weak var txtFirstName: HoshiTextField!
    @IBOutlet weak var txtLastName: HoshiTextField!
    @IBOutlet weak var txtEmail: HoshiTextField!
    
    
    
    
    var codePicker = MRCountryPicker()
    var user:[UserModel]? = nil
    private var db = Firestore.firestore()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialLoad()
    }
    
    func initialLoad(){
        
        self.registerView.isHidden = true
        self.otpView.isHidden = true
        let locale = Locale.current

        if let regionCode = locale.regionCode{
            codePicker.setCountry(regionCode)
            self.txtCode.text = (codePicker.getPhoneCode(regionCode))
        }
        
        
        codePicker.countryPickerDelegate = self
        codePicker.showPhoneNumbers = true
        self.txtCode.inputView = codePicker
        
    }
    
    
    @IBAction func registerUserPressed(_ sender: Any) {
        self.registerView.isHidden = true
        guard let name = self.txtFirstName.text, !name.isEmpty else{
            Alertift.alert(title:AppTitle , message: "First Name should not be empty" )
                           .action(.default("OK"))
                           .show(on: self)
            return
        }
        
        guard let phone = self.txtPhone.text else{
            return
        }
        
        
       
        var tokenData = [["token":"","device":""]]
        
        if let token = Defaults.string(forKey: "fcm"){
            
            if let device = UIDevice.current.identifierForVendor?.uuidString{
                tokenData = [["token":token,"device":device]]
            }
            
           
        }
        
        guard let userId = Defaults.string(forKey: "UID") else{
            return
        }
        
        let params = ["firstName":name,"lastName":self.txtLastName.text ?? "","adminId":"","phone":phone,"countryCode":self.txtCode.text ?? "","type":"client","email":self.txtEmail.text ?? "","userId":userId,"token":tokenData] as [String : Any]
        
       addData(userData: params)
        
        
    }
    
    
    func addData(userData:[String:Any]){
        
        db.collection("users").addDocument(data: userData){ err in
            if let err = err {
                
                print("Error writing document: \(err)")
            } else {
                self.registerView.isHidden = true
                Switcher.updateRootVC()
                //print("Document successfully written!")
            }
        }
        
    }
    
    
    @IBAction func registerCancelPressed(_ sender: Any) {
        self.registerView.isHidden = true
    }
    
    func showRegisterDialog(){
        
        self.registerView.isHidden = false
    }
    
    
    @IBAction func sendOtpPressed(_ sender: Any) {
        
       // self.userEntryCheck()
    
        guard let txtNumber = self.txtPhone.text, !txtNumber.isEmpty else{
            
            Alertift.alert(title:AppTitle , message: "Please enter a valid mobile number" )
                           .action(.default("OK"))
                           .show(on: self)

            
            return
        }
        
     
        
        guard txtNumber.count > 6 else{
            
            Alertift.alert(title:AppTitle , message: "Please enter a valid mobile number" )
                           .action(.default("OK"))
                           .show(on: self)
            
           return
        }
        
        
        if Reachability.isConnectedToNetwork(){
        
            guard let code = self.txtCode.text else{
                return
            }
            
            getLogin(phoneNumber: code + txtNumber )
            
        }else{
            
            self.showNotification(message:noInternet)
        }
        
        
    }
    
    @IBAction func verifyOtpPressed(_ sender: Any) {
        guard let otp = self.txtOtp.text, !otp.isEmpty else{
          self.showNotification(message:"Enter a valid otp")
           return
        }
        
        guard let id = Defaults.string(forKey: "verificationID") else{
            return
        }
        

        self.letsAuthenticate(id: id, code: otp)
    }
    
    
    @IBAction func resendPressed(_ sender: Any) {
        
        if Reachability.isConnectedToNetwork(){
        
            guard let code = self.txtCode.text else{
                return
            }
            self.txtOtp.text = ""
            getLogin(phoneNumber: code + self.txtPhone.text! )
            
        }else{
            
            self.showNotification(message:noInternet)
        }
    }
    
    
    func getLogin(phoneNumber:String){
        SVProgressHUD.show()
       // let phoneNumber = "+919895208433"


        Auth.auth().settings?.isAppVerificationDisabledForTesting = false
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
            
        SVProgressHUD.dismiss()
          if let error = error {
       
           // self.showMessagePrompt(error.localizedDescription)
            return
          }
        
          Defaults.set(verificationID, forKey: "verificationID")
          //self.txtOtp.placeholder = phoneNumber
          self.otpView.isHidden = false
          self.txtOtp.becomeFirstResponder()
          
        }
        
    }
    
    
    
    func letsAuthenticate(id:String,code:String){
        
       // let code = "223344"
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: id,
            verificationCode: code)
        
            Auth.auth().signIn(with: credential) { (userInfo: AuthDataResult?,error: Error?) in
            if let error = error{
                Alertift.alert(title:AppTitle , message: "Please enter a valid OTP" )
                               .action(.default("OK"))
                               .show(on: self)
            }else{
                Defaults.set(userInfo?.user.uid, forKey: "UID")
                self.userEntryCheck()
            }
            
            
        }
        
    }
    
    func userEntryCheck(){
    
        guard let number = self.txtPhone.text else{
            return
        }

        let userInstance = db.collection("users").whereField("phone", isEqualTo: number)

        userInstance.addSnapshotListener { (querySnapshot, error) in
            guard let documents = querySnapshot?.documents else {
            
              return
            }
           // print("dddo",documents)

            if documents.count > 0{

                self.users = documents.compactMap{ (queryDocumentSnapshot) -> UserModel? in
                   // print(queryDocumentSnapshot.data())
                    
                    return try? queryDocumentSnapshot.data(as:UserModel.self)
                }
                
                
                
                if self.users.count == 0{
                    self.registerView.isHidden = false
                }else{
                    
                    guard let firstName = self.users[0].firstName else{
                        return
                    }
                    Defaults.set(firstName, forKey: "firstName")
                    if let lastName = self.users[0].lastName {
                        Defaults.set(lastName, forKey: "lastName")
                    }
                    
                    if let email = self.users[0].email{
                        Defaults.set(email, forKey: "email")
                    }
                    if let phone = self.users[0].phone{
                        
                        Defaults.set(phone, forKey: "phone")
                    }
                    
                   
                    
                    Switcher.updateRootVC()

                    
                }


              //  print("****",self.users.first?.id)
            }else{
                self.registerView.isHidden = false
            }

          }

   }
}

extension SignInViewController:MRCountryPickerDelegate{
    
   
    
    func countryPhoneCodePicker(_ picker: MRCountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        self.txtCode.text = phoneCode
    }
    
    
    
    
}
