//
//  ReportsViewController.swift
//  UrVitals
//
//  Created by Sajin M on 16/07/2021.
//

import UIKit
import FirebaseFirestore
import FirebaseFirestoreSwift
import Kingfisher

class ReportsViewController: UIViewController {
    
    
    @Published var reports = [ReportModel]()
    @Published var users = [UserModel]()
    @Published var hospitals = [HospitalModel]()
    private var db = Firestore.firestore()
 
    @IBOutlet weak var reportTableview: UITableView!
    
    @IBOutlet weak var lbListView: UITableView!
    @IBOutlet weak var labView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.labView.isHidden = true
 
        
        guard let phone =  Defaults.string(forKey: "phone") else
        { return  }
        
        userEntry(phone: phone)
        
        
    }
    
    func userEntry(phone:String)
    
    {

        let userInstance = db.collection("users").whereField("phone", isEqualTo: phone)

        userInstance.addSnapshotListener { (querySnapshot, error) in
            guard let documents = querySnapshot?.documents else {
            
              return
            }
           // print("dddo",documents)

            if documents.count > 0{

                self.users = documents.compactMap{ (queryDocumentSnapshot) -> UserModel? in
                   // print(queryDocumentSnapshot.data())
                    
                    return try? queryDocumentSnapshot.data(as:UserModel.self)
                }
                
               
                    guard let adminId = self.users[0].adminId else{
                        return
                    }
                 
                var admin = [String]()
                for info in self.users{
                    if let id = info.adminId{
                        admin.append(id)
                    }
                }
                
                Defaults.set(admin, forKey: "adminID")
//
                   self.getHospitals(id: admin)
                
            }
          }

   }
    
    func getHospitals(id:[String]){
        
        let hospitalInstance = db.collection("hospitals")

        hospitalInstance.addSnapshotListener { (querySnapshot, error) in
            guard let documents = querySnapshot?.documents else {
            
              return
            }
           // print("dddo",documents)
            var allHospitals = [HospitalModel]()
            if documents.count > 0{

                allHospitals = documents.compactMap{ (queryDocumentSnapshot) -> HospitalModel? in
                   // print(queryDocumentSnapshot.data())
                    
                    return try? queryDocumentSnapshot.data(as:HospitalModel.self)
                }
        
            }
            
            for hospital in allHospitals{
                
                if hospital.contains(id) {
                    self.hospitals.append(hospital)
                }
                
            }
            
            //self.hospitals = allHospitals.filter{ id.contains($0.adminId) }
            
            print(self.hospitals)
        }
    }
    
    @IBAction func showLabList(_ sender: Any) {
        self.lbListView.isHidden = true
    }
    override func viewDidAppear(_ animated: Bool) {
        getData()
    }
    
    @IBAction func closeLabView(_ sender: Any) {
        self.labView.isHidden = true
    }
    
    
    
    func getData(){
        
        guard let uid = Defaults.string(forKey: "UID") else {
            return
        }

        let ReportReference = db.collection("urVitals")
        let userIdReference = db.collection("users").document(uid)
        let query = ReportReference.whereField("category", isEqualTo: "report").whereField("userId", isEqualTo: userIdReference)
        
    
        query.addSnapshotListener() { (querySnapshot, err) in
              if let err = err {
                  print("Error getting documents: \(err)")
              } else {
            
                guard let documents = querySnapshot?.documents else {
                  return
                }
                
                                  for document in querySnapshot!.documents {
                                      print("\(document.documentID) => \(document.data())")
                                  }

                self.reports = documents.compactMap{ (querySnapshot) -> ReportModel? in
                   
                    return try? querySnapshot.data(as:ReportModel.self)
                }
                
               // print(self.reports)
                if self.reports.count > 0{
                    self.reportTableview.delegate = self
                    self.reportTableview.dataSource = self
                    self.reportTableview.reloadData()
                }
                
//                  for document in querySnapshot!.documents {
//                      print("\(document.documentID) => \(document.data())")
//                  }
              }
          }
        
        
    }
    
    
    @IBAction func addReportPressed(_ sender: Any) {
        
        let NewScene = AddReportViewController.instantiate(fromAppStoryboard: .Main)
    
                         if let navigator = self.navigationController {
    
                             navigator.pushViewController(NewScene, animated: true)
                      
                      }
    }
    
    
    

}

extension ReportsViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.reports.count 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = reportTableview.dequeueReusableCell(withIdentifier: "ReportCell", for: indexPath) as! ReportCell
        
        if reports[indexPath.row] != nil{
            
            cell.lblTitle.text = reports[indexPath.row].docs?.reportType
            cell.lblType.text = reports[indexPath.row].docs?.testedFor
            
          
            
            if let date = reports[indexPath.row].uploadDate?.dateValue(){
              
                cell.lblDate.text = date.toString(format: "dd-MMM-yyyy")
            }
            
            if let icon = reports[indexPath.row].docs?.iconUrl{
                
                let url = URL(string: icon)
                cell.imgIcon?.kf.setImage(with: url)
            }
            
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let NewScene = AddReportViewController.instantiate(fromAppStoryboard: .Main)
    
        NewScene.isEdit = true
        NewScene.reports = reports[indexPath.row]
        if let navigator = self.navigationController {
    
        navigator.pushViewController(NewScene, animated: true)
                      
        }
        
    }
    
    
    
    
}
