//
//  MenuViewController.swift
//  UrVitals
//
//  Created by Sajin M on 03/08/2021.
//

import UIKit
import MessageUI
import TextFieldEffects
import FirebaseFirestore
import FirebaseFirestoreSwift
import Firebase
import FirebaseStorage
import Alertift
class MenuViewController: UIViewController {
    
    private var db = Firestore.firestore()
    @IBOutlet weak var provfileView: UIView!
    @IBOutlet weak var lblName: HoshiTextField!
    @IBOutlet weak var lblPhone: HoshiTextField!
    @IBOutlet weak var lblEmail: HoshiTextField!
    
    
    @IBOutlet weak var menuTableView: UITableView!{
        didSet {
            menuTableView.tableFooterView = UIView(frame: .zero)
        }
    }
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.provfileView.isHidden = true
        self.menuTableView.delegate = self
        self.menuTableView.dataSource = self
        self.menuTableView.reloadData()
        setupFields()
        // Do any additional setup after loading the view.
    }
    
    
    func setupFields(){
        
        if let name = Defaults.string(forKey: "firstName"){
            if let lastName = Defaults.string(forKey: "lastName"){
                self.lblName.text = name + " " + lastName
            }else{
                self.lblName.text = name
            }
        }
        
        if let phone = Defaults.string(forKey: "phone"){
            self.lblPhone.text = phone
            self.lblPhone.delegate = self
        }
        
        if let email = Defaults.string(forKey: "email"){
            self.lblEmail.text = email
        }
        
    }
    
    func logOut(){
        
        //Defaults.setValue(nil, forKey: UserToken)
        
        let NewScene = SignInViewController.instantiate(fromAppStoryboard: .Main)
        
        NewScene.hidesBottomBarWhenPushed = true

        if let navigator = self.navigationController {

            navigator.pushViewController(NewScene, animated: true)


        }

        
    }
    
    @IBAction func updateProfilePressed(_ sender: Any) {
        
        guard let uid = Defaults.string(forKey: "UID") else {
            return
        }
        
        guard let name = self.lblName.text, !name.isEmpty else{
            Alertift.alert(title:AppTitle , message: "Name should not be empty" )
                           .action(.default("OK"))
                           .show(on: self)
            return
        }
        
        guard let phone = self.lblPhone.text, !phone.isEmpty else{
            Alertift.alert(title:AppTitle , message: "Phone number should not be empty" )
                           .action(.default("OK"))
                           .show(on: self)
            return
        }
        
        
        
        
        db.collection("users").whereField("userId", isEqualTo: uid).getDocuments() {(snapshot, error) in
                   if let error = error {
                       print("Error getting document \(error)")
                       
                   } else {

                    for document in snapshot!.documents{
                        document.reference.updateData(["firstName": name,"phone":phone,"email":self.lblEmail.text ?? ""])
               
                                    }
                    
                    Defaults.set(name, forKey: "firstName")
                    Defaults.set(phone, forKey: "phone")
                    Defaults.set(self.lblEmail.text, forKey: "email")
                    self.provfileView.isHidden = true
                    
                    Alertift.alert(title:AppTitle , message: "Updated Successfully" )
                                   .action(.default("OK"))
                                   .show(on: self)
    
                       }
               }
        
       
 
    }
    
    
    @IBAction func cancelProfilePressed(_ sender: Any) {
        self.provfileView.isHidden = true
    }
    
    
    func confirmLogout() {

        let optionMenu = UIAlertController(title: nil, message: "Do you want to log out", preferredStyle: .alert)

      
            let yesAction = UIAlertAction(title: "Yes", style: .default) { _ in

                self.logOut()

            }

            let cancelAction = UIAlertAction(title: "No", style: .cancel)

            // 4
          
            optionMenu.addAction(yesAction)
            optionMenu.addAction(cancelAction)

            // 5
            self.present(optionMenu, animated: true, completion: nil)

    }
    
    func whatsappChat(){
        
       

        let phone = "+918590127374"
        let whatsUrl = "whatsapp://send?phone="
        let whatsMessage = "&text=Hi"
        
        var whatsStr = whatsUrl + phone + whatsMessage
        
       
        
        whatsStr = whatsStr.replacingOccurrences(of: "+", with: "")
       
        
        let whatsappURL = URL(string: whatsStr)
        if let whatsappURL = whatsappURL {
            if UIApplication.shared.canOpenURL(whatsappURL) {
                UIApplication.shared.open(whatsappURL)
            }
        }else{
            
        
                   if let url = URL(string: "https://apps.apple.com/in/app/whatsapp-messenger/id310633997"),
                       UIApplication.shared.canOpenURL(url)
                   {
                       if #available(iOS 10.0, *) {
                           UIApplication.shared.open(url, options: [:], completionHandler: nil)
                       } else {
                           UIApplication.shared.openURL(url)
                       }
                   }
            
            
        }
    }
    
    func sendEmail() {
        
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([customerCareEmail])
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }

   
}
extension MenuViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.menuTableView.dequeueReusableCell(withIdentifier: "menuCell", for: indexPath) as! MenuCell
        cell.lblTitle.text = menuTitle[indexPath.row]
        cell.imgIcon.image = UIImage(named: menuIcons[indexPath.row])
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 4
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0{
            self.provfileView.isHidden = false
        }
        
        if indexPath.row == 1{
            sendEmail()
        }
        
        if indexPath.row == 2{
            whatsappChat()
        }
        
        if indexPath.row == 3{
            confirmLogout()
        }
        
    }
    
    
}

extension MenuViewController:MFMailComposeViewControllerDelegate{
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
    
        controller.dismiss(animated: true)
        
        
    }
    
}

extension MenuViewController:UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.lblPhone{
        let maxLength = 10
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        return newString.length <= maxLength
    }
        return true
    }
    
}
