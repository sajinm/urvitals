//
//  AddReportViewController.swift
//  UrVitals
//
//  Created by Sajin M on 17/07/2021.
//

import UIKit
import TextFieldEffects
import VisionKit
import FirebaseFirestore
import FirebaseFirestoreSwift
import Firebase
import FirebaseStorage
import MobileCoreServices
import PDFKit
import Alertift
import SVProgressHUD

class AddReportViewController: BaseViewController {
    
    @Published var categories = [TestsModel]()
    @IBOutlet weak var txtDate: UITextField!
    
    @IBOutlet weak var txtCategory: HoshiTextField!
    @IBOutlet weak var txtReportType: HoshiTextField!
    @IBOutlet weak var reportTypeView: UIView!
    @IBOutlet weak var btnRemove: UIButton!
    @IBOutlet weak var scannedImage: UIImageView!
    @IBOutlet weak var btnScan: UIButton!
    @IBOutlet weak var categoryColletionView: UICollectionView!
    @IBOutlet weak var categoryView: UIView!
    @IBOutlet weak var btnCategoryClose: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    
    @IBOutlet weak var availableImg: UIImageView!
    @IBOutlet weak var btnCategory: UIButton!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    
    var reports:ReportModel?
   
    @available(iOS 11.0, *)
    lazy var pdfDocument = PDFDocument()
    
    var datePicker = UIDatePicker()
    let imagePicker = UIImagePickerController()
    var isEdit:Bool = false
    let dateFormatter: DateFormatter = DateFormatter()
    var finalImageArray:[UIImage] = []
    var typePicker = UIPickerView()
    var typeArray:[String]?
    var selecteTestIcon:String = ""
    var selectedImageUrl:String = ""
    
    private var db = Firestore.firestore()
    @IBOutlet weak var imageCollectionView: UICollectionView!
    var selectedDate:Date?
    let storage = Storage.storage()
    var reportImageUrl:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        self.reportTypeView.isHidden = true
        
        if isEdit {
            self.updateView()
        }
        
       
        let dateFormatter: DateFormatter = DateFormatter()
        let currentDate = Date()
         dateFormatter.dateFormat = "dd-MM-yyyy"
        let fromDate:String = dateFormatter.string(from: currentDate)
        self.selectedDate = currentDate
        self.txtDate.text = fromDate
        self.categoryView.isHidden = true
        
        if #available(iOS 13.4, *) {
            self.datePicker.preferredDatePickerStyle = .wheels // Replace .inline with .compact
           
                    }
        
        dateFormatter.dateFormat = "dd-MM-yyyy"
        self.txtDate.inputView = self.datePicker
        
        datePicker.addTarget(self, action: #selector(self.datePickerFromValueChanged(_:)), for: .valueChanged)

        
        datePicker.datePickerMode = .date
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        //let cancelButton = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton], animated: false)
        
        self.txtDate.inputAccessoryView = toolbar
        
        let query = db.collection("TestedForDatas")
        
        query.addSnapshotListener() { (querySnapshot, err) in
              if let err = err {
                  print("Error getting documents: \(err)")
              } else {
            
                guard let documents = querySnapshot?.documents else {
                  return
                }
                
//                                  for document in querySnapshot!.documents {
//                                      print("\(document.documentID) => \(document.data())")
//                                  }
//
                self.categories = documents.compactMap{ (querySnapshot) -> TestsModel? in
                   
                    return try? querySnapshot.data(as:TestsModel.self)
                }
                
                //print(self.reports[0].filterDate?.dateValue())
                if self.categories.count > 0{
                    self.categoryColletionView.delegate = self
                    self.categoryColletionView.dataSource = self
                    self.categoryColletionView.reloadData()
                }
                
//                  for document in querySnapshot!.documents {
//                      print("\(document.documentID) => \(document.data())")
//                  }
              }
          }
    }
    
    
    func updateView(){
        self.lblTitle.text = "Report"
        self.btnSubmit.isHidden = true
        if let date = reports?.uploadDate?.dateValue(){
            self.txtDate.text = date.toString(format: "dd-MMM-yyyy")
        }
        
        if let test = reports?.docs?.testedFor{
            self.txtCategory.text = test
        }
        
        if let type = reports?.docs?.reportType{
            self.txtReportType.text = type
        }
        
        if let icon = reports?.docs?.iconUrl{
            self.selecteTestIcon = icon
        }
        self.btnCamera.setImage(nil, for: .normal)
        self.txtCategory.isEnabled = false
        self.reportTypeView.isHidden = false
        self.btnCategory.isEnabled = false
        self.txtReportType.isEnabled = false
        self.availableImg.image = UIImage(named: "attach")

    }
    
    @objc func datePickerFromValueChanged(_ sender: UIDatePicker){
        
      
        let dateFormatter: DateFormatter = DateFormatter()
         dateFormatter.dateFormat = "dd-MM-yyyy"
        let selectedDate: String = dateFormatter.string(from: sender.date)
        self.selectedDate = sender.date
        self.txtDate.text = selectedDate
    
        
    }
    
    
    @objc func donedatePicker(){
        
    
        self.view.endEditing(true)
        
    }
    
    
    @IBAction func submitPressed(_ sender: Any) {
        
        guard let docDate = self.selectedDate else {
            return
        }
        
        guard let testType = self.txtCategory.text else{
            
            Alertift.alert(title:AppTitle , message: "Please select Test" )
                           .action(.default("OK"))
                           .show(on: self)
            return
        }
        
        guard let reportType = self.txtReportType.text else{
            
            Alertift.alert(title:AppTitle , message: "Please select Report" )
                           .action(.default("OK"))
                           .show(on: self)
            return
        }
        
    
        
        if finalImageArray.count > 0{
            
            for (index,image) in self.finalImageArray.enumerated(){
                
                let pdfPage = PDFPage(image: image)
               
                if let page = pdfPage{
                    pdfDocument.insert(page, at: index)
                    
                }
 
            }
           
            SVProgressHUD.show()
        
            uploadMedia { (urlArray) in
                guard let uid = Defaults.string(forKey: "UID") else {
                    return
                }
                
                let userIdReference = self.db.collection("users").document(uid)
                let docParams = ["dateOfReport":docDate,"urls":urlArray!,"testedFor":testType,"iconUrl":self.selecteTestIcon,"otherReportType":"","reportType":reportType] as [String:Any]
                
                let params = ["adminId":"","category":"report","filterDate":self.selectedDate as Any,"uploadDate":self.selectedDate,"userId":userIdReference,"docs":docParams,"status":true,"origin":"mobile"] as [String:Any]
                
                self.addReportData(param: params)
            }
            
            
           
            
            
        }else{
            
            Alertift.alert(title:AppTitle , message: "Please select Report Documents" )
                           .action(.default("OK"))
                           .show(on: self)
            
        }
       
    }
    
    
    func addReportData(param:[String:Any]){
        
        if isEdit{
            
            guard let docId = self.reports?.id else{
                return
            }
            
            db.collection("urVitals").document(docId).setData(param){ err in
                if let err = err {
                    print("Error updating document: \(err)")
                } else {
                    print("Document successfully updated")
                }
            }
            

        }else{
            
            db.collection("urVitals").addDocument(data: param).addSnapshotListener { (snapShot, error) in
                SVProgressHUD.dismiss()
                if let error = error {
                    print("Error getting documents: \(error)")
                } else {
              
                    self.backNavigation()
                    print(snapShot?.data())
                  }
                
            }
            
        }
        

        
    }

    @IBAction func scanPressed(_ sender: Any) {
        
        
        if isEdit{
            
            let NewScene = DocViewController.instantiate(fromAppStoryboard: .Main)
            
            guard let url = reports?.docs?.urls?[0].url else{
                return
            }
    
            NewScene.docUrl = url
            if let navigator = self.navigationController {
        
            navigator.pushViewController(NewScene, animated: true)
                          
        }
            
     
        }else{
            
            chooseUploadOption()
        }
        
       
        
       
    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.backNavigation()
        
    }
    
    
    func uploadMedia(completion:@escaping(_ url:[[String:String]]?) -> Void){
        
        var urlArray = [[String:String]]()
        let data = pdfDocument.dataRepresentation()
        let storageRef = storage.reference()
        let riversRef = storageRef.child("uploads/report/report.pdf")

        // Upload the file to the path "images/rivers.jpg"
        let uploadTask = riversRef.putData(data!, metadata: nil) { (metadata, error) in
          guard let metadata = metadata else {
            // Uh-oh, an error occurred!
            return
          }
          // Metadata contains file metadata such as size, content-type.
          let size = metadata.size
          // You can also access to download URL after upload.
          riversRef.downloadURL { (url, error) in
            guard let downloadURL = url else {
              // Uh-oh, an error occurred!
              return
            }
            
            let urls = ["deviceType":"iOS","type":"application/pdf","url":downloadURL.absoluteString]
            
            urlArray.append(urls)
            completion(urlArray)
          }
        }
        
        
        
    }
    

    
    
    @IBAction func reportTypePressed(_ sender: Any) {
        
        
    }
    
    
    @IBAction func categoryClosePressed(_ sender: Any) {
        self.categoryView.isHidden = true
    }
    

    @IBAction func categoryPressed(_ sender: Any) {
        
        
        
       // self.txtPurpose.resignFirstResponder()
        self.view.endEditing(true)
        self.categoryView.isHidden = false

        
    }
    
    func chooseUploadOption() {

        let optionMenu = UIAlertController(title: nil, message: "Choose Receipt", preferredStyle: .actionSheet)

        let fileAction = UIAlertAction(title: "Scan Document", style: .default) { _ in
            
            self.goForscan()


     }

            let galleryAction = UIAlertAction(title: "Gallery", style: .default) { _ in

                let imageMediaType = kUTTypeImage as String
                self.imagePicker.allowsEditing = true
                self.imagePicker.mediaTypes = [imageMediaType]
                self.imagePicker.sourceType = .photoLibrary
                self.imagePicker.delegate = self
                self.present(self.imagePicker, animated: true, completion: nil)

            }

            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)

            // 4
            optionMenu.addAction(fileAction)
            optionMenu.addAction(galleryAction)
            optionMenu.addAction(cancelAction)

            // 5
            self.present(optionMenu, animated: true, completion: nil)

    }
    
    
    
    
    func goForscan(){
          
                      if #available(iOS 13.0, *) {
                          let scannerViewController = VNDocumentCameraViewController()
                          scannerViewController.delegate = self
                          present(scannerViewController, animated: true)

                      } else {


                       
                       imagePicker.allowsEditing = true
                       imagePicker.sourceType = .camera
                       present(imagePicker, animated: true, completion: nil)
                       
                         
                   }
                       
           
           
       }
    
   

}


extension AddReportViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        
        var selectedImage: UIImage?
        var selectedImageUrl: URL?
        
        if let editedImage = info[.editedImage] as? UIImage {
            selectedImage = editedImage
//            let mediaType = info[UIImagePickerController.InfoKey.mediaType] as! CFString
//              if mediaType == kUTTypeImage {
//                selectedImageUrl = info[UIImagePickerController.InfoKey.imageURL] as? URL
//                // Handle your logic here, e.g. uploading file to Cloud Storage for Firebase
//              }
           
            picker.dismiss(animated: true, completion: nil)
        } else if let originalImage = info[.originalImage] as? UIImage {
            selectedImage = originalImage
            
//            let mediaType = info[UIImagePickerController.InfoKey.mediaType] as! CFString
//              if mediaType == kUTTypeImage {
//                selectedImageUrl = info[UIImagePickerController.InfoKey.imageURL] as? URL
//                // Handle your logic here, e.g. uploading file to Cloud Storage for Firebase
//              }
           
            picker.dismiss(animated: true, completion: nil)
        }
        
       
        
        if let image = selectedImage{
            
            self.finalImageArray.append(image)
//            if let url = selectedImageUrl{
//                self.reportImageUrl.append(url)
//            }
//
            
            if self.finalImageArray.count > 0{
                
                self.imageCollectionView.delegate = self
                self.imageCollectionView.dataSource = self
                self.imageCollectionView.reloadData()
                self.imageCollectionView.isHidden = false
                
            }
            
        }
        
     
        
       
        //self.btnRemove.isHidden = false
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
               
               dismiss(animated: true, completion: nil)
    }
    
    
}

@available(iOS 13.0, *)
extension AddReportViewController:VNDocumentCameraViewControllerDelegate{
    
     func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFinishWith scan: VNDocumentCameraScan) {
         // Process the scanned pages
         
         var scannedImage:UIImage?
         
         
         for pageNumber in 0..<scan.pageCount {
            
             let image = scan.imageOfPage(at: pageNumber)
          
           // self.scannedImage.image = image
            
            self.finalImageArray.append(image)
            

       
     }
        
        if self.finalImageArray.count > 0{
            
            self.imageCollectionView.delegate = self
            self.imageCollectionView.dataSource = self
            self.imageCollectionView.reloadData()
            
        }
        
        controller.dismiss(animated: true)
        
     }
     
     func documentCameraViewControllerDidCancel(_ controller: VNDocumentCameraViewController) {
    
         controller.dismiss(animated: true)
     }
     
     func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFailWithError error: Error) {
         // You should handle errors appropriately in your app.
     
         // You are responsible for dismissing the controller.
         controller.dismiss(animated: true)
     }

    
}


extension AddReportViewController:UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        if collectionView == imageCollectionView{
            
            
            return self.finalImageArray.count
        }
        
        return self.categories.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         
         if collectionView == categoryColletionView {
             
            let width = (self.categoryColletionView.frame.size.width) / 4
             let height = width
             return CGSize(width: width, height: height)
             
         }
        
        if collectionView == imageCollectionView{
            
            return CGSize(width: 114.0, height: 114.0)
        }
        
        return CGSize(width: 0, height: 0)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
         let info = categories[indexPath.row]
            
            if let title = info.name{
                
                self.txtCategory.text = title
            
            }
        
        if let icon = info.imgUrln{
            self.selecteTestIcon = icon
        }
        
                  
        
        if let types = categories[indexPath.row].reportType{
            self.typeArray = types
            self.txtReportType.inputView = self.typePicker
            self.typePicker.delegate = self
            self.typePicker.dataSource = self
        }
        
        self.reportTypeView.isHidden = false
        self.categoryView.isHidden = true
        }
           

    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        
        if collectionView == imageCollectionView{
            
            let cell = self.imageCollectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as! ImageCollectionCell
            
            cell.itemPic.image = self.finalImageArray[indexPath.row]
            cell.index = indexPath.row
            cell.delegate = self
            
            return cell
        }
        
        
        
        let cell = self.categoryColletionView.dequeueReusableCell(withReuseIdentifier: CategoryCell, for: indexPath) as! CategoryViewCell
        
         let info = self.categories[indexPath.row]
            
            if let title = info.name{
                
                 cell.lblTitle.text = title
            }
            
            if let icon = info.imgUrln{

                let url = URL(string:icon)

                cell.imgIcon.kf.setImage(with:url)

            }
        return cell
           
        }
        
       
        
    }
    

extension AddReportViewController:imageCellDelegate{
    func removeImage(at index: Int) {
        
        if finalImageArray.count > 0{
            
            self.finalImageArray.remove(at: index)
            self.reportImageUrl.remove(at: index)
            self.imageCollectionView.reloadData()
            
        }
        
       
        
    }
    
    
    
}


extension AddReportViewController : UIPickerViewDelegate,UIPickerViewDataSource{

func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
}

func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {

    return typeArray?.count ?? 0
}
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
          
            self.txtReportType.text = typeArray?[row]
       
      }
      
      

        func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {

            return typeArray?[row]
           

        }
    
    
    
}
