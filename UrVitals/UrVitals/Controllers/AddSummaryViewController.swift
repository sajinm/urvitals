//
//  AddSummaryViewController.swift
//  UrVitals
//
//  Created by Sajin M on 03/08/2021.
//

import UIKit
import TextFieldEffects
import VisionKit
import FirebaseFirestore
import FirebaseFirestoreSwift
import Firebase
import FirebaseStorage
import MobileCoreServices
import PDFKit
import Alertift
import SVProgressHUD


class AddSummaryViewController: BaseViewController {
    
    @Published var categories = [TestsModel]()
   // @IBOutlet weak var txtDate: UITextField!
    
    @IBOutlet weak var reportTypeView: UIView!
   // @IBOutlet weak var btnRemove: UIButton!
    @IBOutlet weak var scannedImage: UIImageView!
    @IBOutlet weak var btnScan: UIButton!
    @IBOutlet weak var categoryColletionView: UICollectionView!
    @IBOutlet weak var categoryView: UIView!
    @IBOutlet weak var btnCategoryClose: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var txtDoctorName: HoshiTextField!
    @IBOutlet weak var txtSpeciality: HoshiTextField!
    
    @IBOutlet weak var availableImg: UIImageView!
   // @IBOutlet weak var btnCategory: UIButton!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var txtAdmitDate: HoshiTextField!
    @IBOutlet weak var txtDischargeDate: HoshiTextField!
    @IBOutlet weak var lbltitle: UILabel!
    
    @IBOutlet weak var btnSubmit: UIButton!
    var prescriptions:ReportModel?
   
    @available(iOS 11.0, *)
    lazy var pdfDocument = PDFDocument()
    
    var datePicker = UIDatePicker()
    var dischargeDatePicker = UIDatePicker()
    let imagePicker = UIImagePickerController()
    var isEdit:Bool = false
    let dateFormatter: DateFormatter = DateFormatter()
    var finalImageArray:[UIImage] = []
    var typePicker = UIPickerView()
    var typeArray:[String]?
    var selecteTestIcon:String = ""
    var selectedImageUrl:String = ""
    
    private var db = Firestore.firestore()
    @IBOutlet weak var imageCollectionView: UICollectionView!
    var selectedDate:Date?
    var admissionDate:Date?
    var dischargeDate:Date?
    let storage = Storage.storage()
    var reportImageUrl:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        if isEdit {
            self.updateView()
        }
        let dateFormatter: DateFormatter = DateFormatter()
        let currentDate = Date()
       
        self.categoryView.isHidden = true
        
        self.datePicker.tag = 0
        self.dischargeDatePicker.tag = 1
        if #available(iOS 13.4, *) {
            self.datePicker.preferredDatePickerStyle = .wheels
            self.dischargeDatePicker.preferredDatePickerStyle = .wheels
        }
        
        
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let fromDate:String = dateFormatter.string(from: currentDate)
        self.selectedDate = currentDate
 
        self.txtAdmitDate.inputView = self.datePicker
        self.txtDischargeDate.inputView = self.dischargeDatePicker
        
        dischargeDatePicker.addTarget(self, action: #selector(self.datePickerFromValueChanged(_:)), for: .valueChanged)
        dischargeDatePicker.datePickerMode = .date
        
        datePicker.addTarget(self, action: #selector(self.datePickerFromValueChanged(_:)), for: .valueChanged)
        datePicker.datePickerMode = .date
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        //let cancelButton = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton], animated: false)
        self.txtAdmitDate.inputAccessoryView = toolbar
        self.txtDischargeDate.inputAccessoryView = toolbar
        let query = db.collection("SpecilaityDatas")
        query.addSnapshotListener() { (querySnapshot, err) in
              if let err = err {
                  print("Error getting documents: \(err)")
              } else {
            
                guard let documents = querySnapshot?.documents else {
                  return
                }

                self.categories = documents.compactMap{ (querySnapshot) -> TestsModel? in
                   
                    return try? querySnapshot.data(as:TestsModel.self)
                }
                
                //print(self.reports[0].filterDate?.dateValue())
                if self.categories.count > 0{
                    self.categoryColletionView.delegate = self
                    self.categoryColletionView.dataSource = self
                    self.categoryColletionView.reloadData()
                }
                

              }
          }
    }
    
    
    func updateView(){
        self.lbltitle.text = "Summary"
        self.btnSubmit.isHidden = true
        if let admDate = prescriptions?.docs?.admitDate?.dateValue(){
            self.txtAdmitDate.text = admDate.toString(format: "dd-MMM-yyyy")
            
            self.admissionDate = admDate
            
        }

        if let disDate = prescriptions?.docs?.dischargeDate?.dateValue(){
            self.txtDischargeDate.text = disDate.toString(format: "dd-MMM-yyyy")
            self.dischargeDate = disDate
        }
        
        
        if let type = prescriptions?.docs?.drName{
            self.txtDoctorName.text = type
        }
        
        if let specility = prescriptions?.docs?.speciality{
            self.txtSpeciality.text = specility
        }
        
        if let icon = prescriptions?.docs?.iconUrl{
            self.selecteTestIcon = icon
        }
        self.btnCamera.setImage(nil, for: .normal)
        
        
       // self.btnCategory.isEnabled = false
        self.txtDoctorName.isEnabled = false
        self.availableImg.image = UIImage(named: "attach")

    }
    
    @objc func datePickerFromValueChanged(_ sender: UIDatePicker){
        let dateFormatter: DateFormatter = DateFormatter()
         dateFormatter.dateFormat = "dd-MM-yyyy"
        
        switch sender.tag {
        case 0:
            
            let selectedDate: String = dateFormatter.string(from: sender.date)
            self.txtAdmitDate.text = selectedDate
            self.admissionDate = sender.date
           
        case 1:
            
            let selectedDate: String = dateFormatter.string(from: sender.date)
            self.txtDischargeDate.text = selectedDate
            self.dischargeDate = sender.date
            
        default:
            break
        }
      
        
        
    
        
    }
    
    
    @objc func donedatePicker(){
        
    
        self.view.endEditing(true)
        
    }
    
    
    @IBAction func submitPressed(_ sender: Any) {
      
        
        guard let docName = self.txtDoctorName.text else{

            Alertift.alert(title:AppTitle , message: "Please Enter name of the doctor" )
                           .action(.default("OK"))
                           .show(on: self)
            return
        }
        
        guard let adDate = self.txtAdmitDate.text else{
            
            Alertift.alert(title:AppTitle , message: "Please Enter Admission Date" )
                           .action(.default("OK"))
                           .show(on: self)
            return
        }
        
        guard let disDate = self.txtDischargeDate.text else{
            
            Alertift.alert(title:AppTitle , message: "Please Enter Discharge" )
                           .action(.default("OK"))
                           .show(on: self)
            return
        }
        
        
        guard let speciality = self.txtSpeciality.text else{
            
            Alertift.alert(title:AppTitle , message: "Please Enter Speciality" )
                           .action(.default("OK"))
                           .show(on: self)
            return
        }
        
        
        
        
        if finalImageArray.count > 0{
            
            for (index,image) in self.finalImageArray.enumerated(){
                
                let pdfPage = PDFPage(image: image)
               
                if let page = pdfPage{
                    pdfDocument.insert(page, at: index)
                    
                }
 
            }
           
            SVProgressHUD.show()
            
            uploadMedia { (urlArray) in
                guard let uid = Defaults.string(forKey: "UID") else {
                    return
                }
                
                let userIdReference = self.db.collection("users").document(uid)
                let docParams = ["admitDate":self.admissionDate,"urls":urlArray!,"iconUrl":self.selecteTestIcon,"drName":docName,"speciality":speciality,"otherSpeciality":"","dischargeDate":self.dischargeDate] as [String:Any]
                
                
                
                let params = ["adminId":"","category":"discharge","filterDate":self.selectedDate as Any,"uploadDate":self.selectedDate,"userId":userIdReference,"docs":docParams,"status":true,"origin":"mobile"] as [String:Any]
                
                self.addReportData(param: params)
            }
            
         
            
        }else{
            
            Alertift.alert(title:AppTitle , message: "Please select Discharge Documents" )
                           .action(.default("OK"))
                           .show(on: self)
            
        }
       
    }
    
    
    func addReportData(param:[String:Any]){
        
        db.collection("urVitals").addDocument(data: param).addSnapshotListener { (snapShot, error) in
            SVProgressHUD.dismiss()
            if let error = error {
                print("Error getting documents: \(error)")
            } else {
          
                print(snapShot?.data())
                self.backNavigation()
              }
            
        }
        
    }

    @IBAction func scanPressed(_ sender: Any) {
        
        
        if isEdit{
            
            let NewScene = DocViewController.instantiate(fromAppStoryboard: .Main)
            
            guard let url = prescriptions?.docs?.urls?[0].url else{
                return
            }
    
            NewScene.docUrl = url
            if let navigator = self.navigationController {
        
            navigator.pushViewController(NewScene, animated: true)
                          
                          }
            
     
        }else{
            
            chooseUploadOption()
        }
        
       
        
       
    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.backNavigation()
        
    }
    
    
    func uploadMedia(completion:@escaping(_ url:[[String:String]]?) -> Void){
        
        var urlArray = [[String:String]]()
        let data = pdfDocument.dataRepresentation()
        let storageRef = storage.reference()
        let riversRef = storageRef.child("uploads/prescription/prescription.pdf")

        // Upload the file to the path "images/rivers.jpg"
        let uploadTask = riversRef.putData(data!, metadata: nil) { (metadata, error) in
          guard let metadata = metadata else {
            // Uh-oh, an error occurred!
            return
          }
          // Metadata contains file metadata such as size, content-type.
          let size = metadata.size
          // You can also access to download URL after upload.
          riversRef.downloadURL { (url, error) in
            guard let downloadURL = url else {
              // Uh-oh, an error occurred!
              return
            }
            
            let urls = ["deviceType":"iOS","type":"application/pdf","url":downloadURL.absoluteString]
            
            urlArray.append(urls)
            completion(urlArray)
          }
        }
        
        
        
    }
    

    
    
    @IBAction func reportTypePressed(_ sender: Any) {
        
        
    }
    
    
    @IBAction func categoryClosePressed(_ sender: Any) {
        self.categoryView.isHidden = true
    }
    

    @IBAction func categoryPressed(_ sender: Any) {
        
 
        self.view.endEditing(true)
        self.categoryView.isHidden = false

    }
    
    func chooseUploadOption() {

        let optionMenu = UIAlertController(title: nil, message: "Choose Receipt", preferredStyle: .actionSheet)

        let fileAction = UIAlertAction(title: "Scan Document", style: .default) { _ in
            
            self.goForscan()


     }

            let galleryAction = UIAlertAction(title: "Gallery", style: .default) { _ in

                let imageMediaType = kUTTypeImage as String
                self.imagePicker.allowsEditing = true
                self.imagePicker.mediaTypes = [imageMediaType]
                self.imagePicker.sourceType = .photoLibrary
                self.imagePicker.delegate = self
                self.present(self.imagePicker, animated: true, completion: nil)

            }

            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)

            // 4
            optionMenu.addAction(fileAction)
            optionMenu.addAction(galleryAction)
            optionMenu.addAction(cancelAction)

            // 5
            self.present(optionMenu, animated: true, completion: nil)

    }
    
    
    
    
    func goForscan(){
          
                      if #available(iOS 13.0, *) {
                          let scannerViewController = VNDocumentCameraViewController()
                          scannerViewController.delegate = self
                          present(scannerViewController, animated: true)

                      } else {


                       
                       imagePicker.allowsEditing = true
                       imagePicker.sourceType = .camera
                       present(imagePicker, animated: true, completion: nil)
                       
                         
                   }
                       
           
           
       }
    
   

}


extension AddSummaryViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        
        var selectedImage: UIImage?
        var selectedImageUrl: URL?
        
        if let editedImage = info[.editedImage] as? UIImage {
            selectedImage = editedImage

            picker.dismiss(animated: true, completion: nil)
        } else if let originalImage = info[.originalImage] as? UIImage {
            selectedImage = originalImage
            
           
            picker.dismiss(animated: true, completion: nil)
        }
        
       
        
        if let image = selectedImage{
            
            self.finalImageArray.append(image)
//            if let url = selectedImageUrl{
//                self.reportImageUrl.append(url)
//            }
//
            
            if self.finalImageArray.count > 0{
                
                self.imageCollectionView.delegate = self
                self.imageCollectionView.dataSource = self
                self.imageCollectionView.reloadData()
                self.imageCollectionView.isHidden = false
                
            }
            
        }
        
     
        
       
        //self.btnRemove.isHidden = false
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
               
               dismiss(animated: true, completion: nil)
    }
    
    
}

@available(iOS 13.0, *)
extension AddSummaryViewController:VNDocumentCameraViewControllerDelegate{
    
     func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFinishWith scan: VNDocumentCameraScan) {
         // Process the scanned pages
         
         var scannedImage:UIImage?
         
         
         for pageNumber in 0..<scan.pageCount {
            
             let image = scan.imageOfPage(at: pageNumber)
          
           // self.scannedImage.image = image
            
            self.finalImageArray.append(image)
            

       
     }
        
        if self.finalImageArray.count > 0{
            
            self.imageCollectionView.delegate = self
            self.imageCollectionView.dataSource = self
            self.imageCollectionView.reloadData()
            
        }
        
        controller.dismiss(animated: true)
        
     }
     
     func documentCameraViewControllerDidCancel(_ controller: VNDocumentCameraViewController) {
    
         controller.dismiss(animated: true)
     }
     
     func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFailWithError error: Error) {
         // You should handle errors appropriately in your app.
     
         // You are responsible for dismissing the controller.
         controller.dismiss(animated: true)
     }

    
}


extension AddSummaryViewController:UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        if collectionView == imageCollectionView{
            
            
            return self.finalImageArray.count
        }
        
        return self.categories.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         
         if collectionView == categoryColletionView {
             
            let width = (self.categoryColletionView.frame.size.width) / 4
             let height = width
             return CGSize(width: width, height: height)
             
         }
        
        if collectionView == imageCollectionView{
            
            return CGSize(width: 114.0, height: 114.0)
        }
        
        return CGSize(width: 0, height: 0)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
         let info = categories[indexPath.row]
            
            if let title = info.name{
                
                self.txtSpeciality.text = title
            
            }
        
        if let icon = info.imgUrln{
            self.selecteTestIcon = icon
        }
        
                  
        
//        if let types = categories[indexPath.row].reportType{
//            self.typeArray = types
//            self.txtReportType.inputView = self.typePicker
//            self.typePicker.delegate = self
//            self.typePicker.dataSource = self
//        }
        
       // self.reportTypeView.isHidden = false
        self.categoryView.isHidden = true
        }
           

    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        
        if collectionView == imageCollectionView{
            
            let cell = self.imageCollectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as! ImageCollectionCell
            
            cell.itemPic.image = self.finalImageArray[indexPath.row]
            cell.index = indexPath.row
            cell.delegate = self
            
            return cell
        }
        
        
        
        let cell = self.categoryColletionView.dequeueReusableCell(withReuseIdentifier: CategoryCell, for: indexPath) as! CategoryViewCell
        
         let info = self.categories[indexPath.row]
            
            if let title = info.name{
                
                 cell.lblTitle.text = title
            }
            
            if let icon = info.imgUrln{

                let url = URL(string:icon)

                cell.imgIcon.kf.setImage(with:url)

            }
        return cell
           
        }
        
       
        
    }
    

extension AddSummaryViewController:imageCellDelegate{
    func removeImage(at index: Int) {
        
        if finalImageArray.count > 0{
            
            self.finalImageArray.remove(at: index)
            self.reportImageUrl.remove(at: index)
            self.imageCollectionView.reloadData()
            
        }
        
       
        
    }
    
    
    
}


//extension AddPrescriptionViewController : UIPickerViewDelegate,UIPickerViewDataSource{
//
//func numberOfComponents(in pickerView: UIPickerView) -> Int {
//    return 1
//}
//
//func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
//
//    return typeArray?.count ?? 0
//}
//
//    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//
//
//            self.txtReportType.text = typeArray?[row]
//
//
//      }
//
//
//
//        func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//
//
//
//            return typeArray?[row]
//
//
//        }
//
//
//
//}
//
