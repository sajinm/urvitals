//
//  PrescriptionViewController.swift
//  UrVitals
//
//  Created by Sajin M on 19/07/2021.
//

import UIKit
import FirebaseFirestore
import FirebaseFirestoreSwift
import Kingfisher

class PrescriptionViewController: UIViewController {
    
    
    @Published var reports = [ReportModel]()
    private var db = Firestore.firestore()
 
    @IBOutlet weak var reportTableview: UITableView!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

    }
    
    override func viewDidAppear(_ animated: Bool) {
        getData()
    }
    
    func getData(){
        
        guard let uid = Defaults.string(forKey: "UID") else {
            return
        }

        let ReportReference = db.collection("urVitals")
        let userIdReference = db.collection("users").document(uid)
        let query = ReportReference.whereField("category", isEqualTo: "prescription").whereField("userId", isEqualTo: userIdReference)
      
        query.addSnapshotListener() { (querySnapshot, err) in
              if let err = err {
                  print("Error getting documents: \(err)")
              } else {
            
                guard let documents = querySnapshot?.documents else {
                  return
                }


                self.reports = documents.compactMap{ (querySnapshot) -> ReportModel? in
                   
                    return try? querySnapshot.data(as:ReportModel.self)
                }
                
                
                if self.reports.count > 0{
                    self.reportTableview.delegate = self
                    self.reportTableview.dataSource = self
                    self.reportTableview.reloadData()
                }
                
//                  for document in querySnapshot!.documents {
//                      print("\(document.documentID) => \(document.data())")
//                  }
              }
          }
    }
    
    @IBAction func addPrescriptionPressed(_ sender: Any) {
        
        let NewScene = AddPrescriptionViewController.instantiate(fromAppStoryboard: .Main)
       
                         
                         if let navigator = self.navigationController {
                        
                         
                             navigator.pushViewController(NewScene, animated: true)
                         
                  
                      }
    }
    
    
    

}

extension PrescriptionViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.reports.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = reportTableview.dequeueReusableCell(withIdentifier: "PrescriptionCell", for: indexPath) as! PrescriptionCell
        
        if reports[indexPath.row] != nil{
            
            cell.lblTitle.text = reports[indexPath.row].docs?.illness
         
            if let speciality = reports[indexPath.row].docs?.speciality{
                cell.lblType.text = speciality
            }
            
            
           // print("****",reports[indexPath.row].uploadDate?.dateValue())
            
            if let date = reports[indexPath.row].docs?.pDate?.dateValue(){
                
               // print(date)
                
                cell.lblDate.text = date.toString(format: "dd-MMM-yyyy")
            }
            if let docName = reports[indexPath.row].docs?.drName{
                
                cell.lblDocName.text = "Dr: " + docName
            }
            
            if let icon = reports[indexPath.row].docs?.iconUrl{
                
                let url = URL(string: icon)
                cell.imgIcon?.kf.setImage(with: url)
            }
            
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let NewScene = AddPrescriptionViewController.instantiate(fromAppStoryboard: .Main)
    
        NewScene.isEdit = true
        NewScene.prescriptions = reports[indexPath.row]
        if let navigator = self.navigationController {
    
        navigator.pushViewController(NewScene, animated: true)
                      
        }
        
    }
    
    
    
}
