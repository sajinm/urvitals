//
//  DocViewController.swift
//  UrVitals
//
//  Created by Sajin M on 26/07/2021.
//

import UIKit
import WebKit

class DocViewController: UIViewController {
    
    var docUrl:String? = nil
    
    @IBOutlet weak var docView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let url = self.docUrl else{
            return
        }
        
        loadDoc(url: url)

        // Do any additional setup after loading the view.
    }
    
    
    func loadDoc(url:String){
        
        let link = URL(string:url)!
//        let request = URLRequest(url: link)
//        print("*****",url)
//        docView.load(request)
        
        if let data = try? Data(contentsOf: link) {
            docView.load(data, mimeType: "application/pdf", characterEncodingName: "", baseURL: link)
            }
        
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    


}
