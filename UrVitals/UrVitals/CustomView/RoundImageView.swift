//
//  RoundImageView.swift
//  OGO
//
//  Created by Sajin M on 07/01/2020.
//  Copyright © 2020 Sajin M. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class RoundImageView: UIImageView {

    @IBInspectable var isRoundedCorners: Bool = false 
    
    @IBInspectable var borderColor: UIColor = .clear
    
    @IBInspectable var borderWidth: CGFloat = 0.0

    override func layoutSubviews() {
        super.layoutSubviews()

        if isRoundedCorners {
//            let shapeLayer = CAShapeLayer()
//            shapeLayer.path = UIBezierPath(ovalIn:
//                CGRect(x: bounds.origin.x, y: bounds.origin.y, width: bounds.width, height: bounds.height
//            )).cgPath
//           // layer.mask = shapeLayer
            layer.cornerRadius = self.frame.height/2
            layer.borderColor = borderColor.cgColor
            layer.borderWidth = borderWidth
            self.clipsToBounds = true
        
        }
        else {
            layer.mask = nil
        }

    }

}

