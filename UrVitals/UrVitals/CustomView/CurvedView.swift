//
//  CurvedView.swift
//  EasyHouz
//
//  Created by Sajin M on 13/05/2020.
//  Copyright © 2020 Netstager. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class CurvedView: UIView {
    
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderColor: UIColor = .clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable
       var shadowRadius: CGFloat {
           get {
               return layer.shadowRadius
           }
           set {
               layer.masksToBounds = false
               layer.shadowRadius = newValue
           }
       }

       @IBInspectable
       var shadowOpacity: Float {
           get {
               return layer.shadowOpacity
           }
           set {
               layer.masksToBounds = false
               layer.shadowOpacity = newValue
           }
       }

       @IBInspectable
       var shadowOffset: CGSize {
           get {
               return layer.shadowOffset
           }
           set {
               layer.masksToBounds = false
               layer.shadowOffset = newValue
           }
       }

       @IBInspectable
       var shadowColor: UIColor? {
           get {
               if let color = layer.shadowColor {
                   return UIColor(cgColor: color)
               }
               return nil
           }
           set {
               if let color = newValue {
                   layer.shadowColor = color.cgColor
               } else {
                   layer.shadowColor = nil
               }
           }
       }
    
    
    override func awakeFromNib() {
          super.awakeFromNib()
         
      }
      override init(frame: CGRect) {
          super.init(frame: frame)
         
      }
      
      required init?(coder aDecoder: NSCoder) {
          super.init(coder: aDecoder)
      }
    
}


