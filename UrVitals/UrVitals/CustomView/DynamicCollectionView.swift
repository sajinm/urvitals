//
//  DynamicCollectionView.swift
//  OGO
//
//  Created by Sajin M on 24/12/2019.
//  Copyright © 2019 Sajin M. All rights reserved.
//

import Foundation
import UIKit

class DynamicHeightCollectionView: UICollectionView {
    override func layoutSubviews() {
        super.layoutSubviews()
        if !__CGSizeEqualToSize(bounds.size, self.intrinsicContentSize) {
            self.invalidateIntrinsicContentSize()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        return contentSize
    }
}
