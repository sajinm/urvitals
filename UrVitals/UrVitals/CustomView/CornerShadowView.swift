//
//  CornerShadowView.swift
//  OGO
//
//  Created by Sajin M on 09/01/2020.
//  Copyright © 2020 Sajin M. All rights reserved.
//

import Foundation
import UIKit

class CornerShadowView:UIView {
    
    
    override init(frame: CGRect) {
       super.init(frame: frame)
       setupView()
     }
     
     //initWithCode to init view from xib or storyboard
     required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
       setupView()
     }

    
    private func setupView() {
        
        self.layer.cornerRadius = 5
          self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.2
          self.layer.shadowOffset = .zero
        self.layer.shadowRadius = 2
         // self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
          self.layer.shouldRasterize = true
          self.layer.rasterizationScale = UIScreen.main.scale
     }
    
}
