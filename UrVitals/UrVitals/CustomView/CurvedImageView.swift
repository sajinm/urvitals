//
//  CurvedImageView.swift
//  OGO
//
//  Created by Sajin M on 22/01/2020.
//  Copyright © 2020 Sajin M. All rights reserved.
//

import UIKit



@IBDesignable
class CurvedImageView:UIImageView {
    
}

extension UIImageView {
    
    
    
    
    
//    @IBInspectable
//    var topRadius: CGFloat {
//      get {
//        return layer.cornerRadius
//      }
//      set {
//
//        
//        var corners = UIRectCorner()
//        corners = [.topRight,.topLeft]
//        
//        let maskPath = UIBezierPath(roundedRect: bounds,
//                                            byRoundingCorners: corners,
//                                            cornerRadii: CGSize(width: newValue, height: newValue))
//                let shape = CAShapeLayer()
//                shape.path = maskPath.cgPath
//                layer.mask = shape
//        
//        
//      }
//    }
//
    @IBInspectable
       var fullCurve: CGFloat {
         get {
           return layer.cornerRadius
         }
         set {
          
            layer.cornerRadius = newValue
           
            
         }
       }
    
    
    func setRounded(borderWidth: CGFloat = 0.0, borderColor: UIColor = UIColor.clear) {
        layer.cornerRadius = frame.width / 2
        layer.masksToBounds = true
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor
    }
       
  //
    
    
    
    
    
//    public func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
//        let maskPath = UIBezierPath(roundedRect: bounds,
//                                    byRoundingCorners: corners,
//                                    cornerRadii: CGSize(width: radius, height: radius))
//        let shape = CAShapeLayer()
//        shape.path = maskPath.cgPath
//        layer.mask = shape
//    }
}
