//
//  CurvedButton.swift
//  OGO
//
//  Created by Sajin M on 24/12/2019.
//  Copyright © 2019 Sajin M. All rights reserved.
//


import Foundation
import UIKit

@IBDesignable class CurvedButton: UIButton {
    
  
    @IBInspectable var cornerRadius: CGFloat = 5 {
        didSet {
            refreshCorners(value: cornerRadius)
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.init(cgColor: UIColor.clear.cgColor){
        didSet {
            setBorderColor(value: borderColor)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    
    override func prepareForInterfaceBuilder() {
        sharedInit()
    }
    
    func sharedInit() {
        
        refreshCorners(value: cornerRadius)
        // Common logic goes here
    }
    
    
    func refreshCorners(value: CGFloat) {
        layer.cornerRadius = value
    }
    
    func setBorderColor(value: UIColor) {
        
        layer.borderWidth = 1.0
        layer.borderColor = value.cgColor
        
    }
}
