//
//  RoundedView.swift
//  OGO
//
//  Created by Sajin M on 15/01/2020.
//  Copyright © 2020 Sajin M. All rights reserved.
//

import Foundation
import UIKit



@IBDesignable class RoundedView: UIView  {
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            refreshCorners(value: cornerRadius)
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.init(cgColor: UIColor.clear.cgColor){
        didSet {
            setBorderColor(value: borderColor)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    
    override func prepareForInterfaceBuilder() {
        sharedInit()
    }
    
    func sharedInit() {
        
        refreshCorners(value: cornerRadius)
        // Common logic goes here
    }
    
    
    func refreshCorners(value: CGFloat) {
        layer.cornerRadius = value
    }
    
    func setBorderColor(value: UIColor) {
        
        layer.borderWidth = 1.0
        layer.borderColor = value.cgColor
        
    }
}
