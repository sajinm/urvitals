//
//  StringExt.swift
//  ExpensePRO
//
//  Created by Sajin M on 03/08/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import Foundation



extension String{
    
    
    
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    
    
    func getTimeComponentString(olderDate older: Date,newerDate newer: Date) -> (String?)  {
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .short

        let componentsLeftTime = Calendar.current.dateComponents([.minute , .hour , .day,.month, .weekOfMonth,.year], from: older, to: newer)

        let year = componentsLeftTime.year ?? 0
        if  year > 0 {
            formatter.allowedUnits = [.year]
            return formatter.string(from: older, to: newer)
        }


        let month = componentsLeftTime.month ?? 0
        if  month > 0 {
            formatter.allowedUnits = [.month]
            return formatter.string(from: older, to: newer)
        }

        let weekOfMonth = componentsLeftTime.weekOfMonth ?? 0
        if  weekOfMonth > 0 {
            formatter.allowedUnits = [.weekOfMonth]
            return formatter.string(from: older, to: newer)
        }

        let day = componentsLeftTime.day ?? 0
        if  day > 0 {
            formatter.allowedUnits = [.day]
            return formatter.string(from: older, to: newer)
        }

        let hour = componentsLeftTime.hour ?? 0
        if  hour > 0 {
            formatter.allowedUnits = [.hour]
            return formatter.string(from: older, to: newer)
        }

        let minute = componentsLeftTime.minute ?? 0
        if  minute > 0 {
            formatter.allowedUnits = [.minute]
            print(formatter.string(from: older, to: newer)!)
            
            return formatter.string(from: older, to: newer) ?? ""
        }

        return nil
    }
    
    
    func getRandom(random range: Range<Int>) -> String {

          let offset: Int
          if range.startIndex < 0 {
              offset = abs(range.startIndex)
          } else {
              offset = 0
          }

          let min = UInt32(range.startIndex + offset)
          let max = UInt32(range.endIndex   + offset)

          return "\(Int(min + arc4random_uniform(max - min)) - offset)"
      }
    
    func getDate() -> String{
        
        
       let dateFormatter = DateFormatter()
       dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
       //dateFormatter.timeZone = TimeZone(abbreviation: "UTC")

       let dt = dateFormatter.date(from: self)
       dateFormatter.timeZone = TimeZone.current
       dateFormatter.dateFormat = "d MMM yyyy"
        
        return dateFormatter.string(from: dt!)
        
    }
    
    func getTime() -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")

        let dt = dateFormatter.date(from: self)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "h:mm a"
         
         return dateFormatter.string(from: dt!)

        
    }
    
    func getMonth() -> String {
        
        let dateFormatter = DateFormatter()
              dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
              dateFormatter.timeZone = TimeZone(abbreviation: "UTC")

              let dt = dateFormatter.date(from: self)
              dateFormatter.timeZone = TimeZone.current
              dateFormatter.dateFormat = "MMMM"
               
               return dateFormatter.string(from: dt!)
        
        
    }
    
    
    func ddmmyyFormat(date:String) -> String{
        
        
        
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "MMM dd, yyyy"

        if let date = inputFormatter.date(from: date) {

            let outputFormatter = DateFormatter()
          outputFormatter.dateFormat = "dd-MM-yyyy"
            
           return outputFormatter.string(from: date)
            
        }

       return date
        
    }
    
 
    func customLocalized(_ lang:String) ->String {

        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        let bundle = Bundle(path: path!)

        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
    
    
    
    func isValidEmail() -> Bool{
        
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
        
        
    }
    
    func isValidMobile() -> Bool{
        
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let matches = detector.matches(in: self, options: [], range: NSMakeRange(0, self.count))
            if let res = matches.first {
                return res.resultType == .phoneNumber && res.range.location == 0 && res.range.length == self.count
            } else {
                return false
            }
        } catch {
            return false
        }
        
        
    }
    
    
    func getCurrencySymbol(currencyCode:String) -> String{
        
        
       
        let curr = Locale.availableIdentifiers.map{ Locale(identifier: $0)}.filter { return currencyCode == $0.currencyCode }.map { ($0.currencySymbol) }.flatMap {$0}.first
        
        if let currencySymbol = curr {
            
            return currencySymbol
            
        }else{
            
            return currencyCode
        }
       
        return ""
        
    }
    

//        func strikeThrough() -> NSAttributedString {
//            let attributeString =  NSMutableAttributedString(string: self)
//            attributeString.addAttribute(
//                NSAttributedString.Key.strikethroughStyle,
//                   value: NSUnderlineStyle.single.rawValue,
//                       range:NSMakeRange(0,attributeString.length))
//            return attributeString
//        }
    
    
  
        func capitalizingFirstLetter() -> String {
          return prefix(1).uppercased() + self.lowercased().dropFirst()
        }

        mutating func capitalizeFirstLetter() {
          self = self.capitalizingFirstLetter()
        }
    
    
    var localized: String {
           return NSLocalizedString(self, comment: "")
       }
    
    
    func isNumeric() -> Bool {
       
            return range(of: "(^-?[\\d]+$)|(-?[\\d]+[.,]{1}[\\d]+$)",
                         options: String.CompareOptions.regularExpression, range: nil, locale: nil) != nil
    }
    
    
}

extension String {

    var length: Int {
        return count
    }

    subscript (i: Int) -> String {
        return self[i ..< i + 1]
    }

    func substring(fromIndex: Int) -> String {
        return self[min(fromIndex, length) ..< length]
    }

    func substring(toIndex: Int) -> String {
        return self[0 ..< max(0, toIndex)]
    }

    subscript (r: Range<Int>) -> String {
        let range = Range(uncheckedBounds: (lower: max(0, min(length, r.lowerBound)),
                                            upper: min(length, max(0, r.upperBound))))
        let start = index(startIndex, offsetBy: range.lowerBound)
        let end = index(start, offsetBy: range.upperBound - range.lowerBound)
        return String(self[start ..< end])
    }
}
