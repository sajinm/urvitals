//
//  SnapshotExtensions.swift
//  UrVitals
//
//  Created by Sajin M on 01/07/2021.
//

import Foundation
import FirebaseFirestore

extension QueryDocumentSnapshot{
    
    func decoded<T:Decodable>() throws -> T {
        
        let jsonData = try JSONSerialization.data(withJSONObject: data(), options:[])
        let object = try JSONDecoder().decode(T.self, from: jsonData)
        
        return object
    }
    
}


extension QuerySnapshot{
    
    func decoded<T:Decodable>() throws -> [T] {
        
        let objects:[T] = try documents.map({try $0.decoded()})
        
        return objects
        
    }
}
