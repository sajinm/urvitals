//
//  AppDelegate.swift
//  UrVitals
//
//  Created by Sajin M on 27/05/2021.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import FirebaseMessaging

@main
class AppDelegate: UIResponder,UIApplicationDelegate,UNUserNotificationCenterDelegate {


    var window: UIWindow?
    
    
    override init() {
        
        FirebaseApp.configure()
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
      //  FirebaseApp.configure()
        
  
        if #available(iOS 10.0, *) {
              // For iOS 10 display notification (sent via APNS)
              UNUserNotificationCenter.current().delegate = self
              
              let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
              UNUserNotificationCenter.current().requestAuthorization(
                  options: authOptions,
                  completionHandler: {_, _ in })
          } else {
              let settings: UIUserNotificationSettings =
                  UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
              application.registerUserNotificationSettings(settings)
          }
          
           UIApplication.shared.applicationIconBadgeNumber = 0
          application.registerForRemoteNotifications()
        
       
        Messaging.messaging().delegate = self
        IQKeyboardManager.shared.enable = true
        Switcher.updateRootVC()
        
        


        return true
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return Auth.auth().canHandle(url)
    }
    
    
    
    func application(_ application: UIApplication,
                     didReceiveRemoteNotification notification: [AnyHashable : Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if Auth.auth().canHandleNotification(notification) {
            completionHandler(.noData)
            return
        }
        // This notification is not auth related, developer should handle it.
       // handleNotification(notification)
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
       print("active")
    }

    func applicationWillTerminate(_ application: UIApplication) {
       
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let firebaseAuth = Auth.auth()

            //At development time we use .sandbox
            firebaseAuth.setAPNSToken(deviceToken, type: AuthAPNSTokenType.unknown)
          // let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
         //  print(deviceTokenString)
       }
    

}

extension AppDelegate :MessagingDelegate{
    
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {

        
        let dataDict:[String: String] = ["token": fcmToken ?? ""]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        print("fcm :", fcmToken ?? "")
          
        print(dataDict)
        Defaults.set(fcmToken, forKey: "fcm")
        
    }

    
}
