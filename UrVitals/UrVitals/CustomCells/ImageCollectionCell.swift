//
//  ImageCollectionCell.swift
//  ExpensePRO
//
//  Created by Sajin M on 03/10/2020.
//  Copyright © 2020 Codelattice. All rights reserved.
//

import UIKit

protocol imageCellDelegate {
    func removeImage(at index:Int)
}

class ImageCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var itemPic: UIImageView!
    
    var index:Int?
    var delegate:imageCellDelegate?
    
    
    @IBAction func removePressed(_ sender: Any) {
        
        if let index = index {
            
            delegate?.removeImage(at: index)
        }
        
    }
    
    
    
}
