//
//  HospitalModel.swift
//  UrVitals
//
//  Created by Sajin M on 03/11/2021.
//

import Foundation

struct HospitalModel:Codable{
    var adminId:String?
    var bTagLine:String?
    var colorAccent:String?
    var colorSecodary:String?
    var hName:String?
    var isActive:String?
    var mainLogo:String?
    
}
