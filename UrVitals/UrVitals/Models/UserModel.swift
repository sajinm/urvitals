//
//  UserModel.swift
//  UrVitals
//
//  Created by Sajin M on 30/06/2021.
//

import Foundation
import FirebaseFirestoreSwift
import FirebaseFirestore

struct UserModel:Codable{
    
    @DocumentID var id:String?
    let adminId:String?
    let age:String?
    let firstName:String?
    let gender:String?
    let lastName:String?
    let email:String?
    let phone:String?
    let token:[tokenData]?
    let type:String?
    let userId:String?
//    let registerDate:Timestamp
//    let updatedDate:Timestamp
    

    
}

struct tokenData:Codable{
    
    var device:String?
    var token:String
}

