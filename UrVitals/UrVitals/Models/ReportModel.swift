//
//  ReportModel.swift
//  UrVitals
//
//  Created by Sajin M on 17/07/2021.
//

import Foundation
import FirebaseFirestoreSwift
import FirebaseFirestore

struct ReportModel:Codable{
    
    @DocumentID var id:String?
    var adminId:String?
    var category:String?
    var filterDate:Timestamp?
    var docs:docModel?
    var origin:String?
    var status:Bool?
    var uploadDate:Timestamp?
}

struct docModel:Codable{
    var pDate:Timestamp?
    var drName:String?
    var dateOfReport:Timestamp?
    var uploadDate:Timestamp?
    var iconUrl:String?
    var reportType:String?
    var testedFor:String?
    var illness:String?
    var speciality:String?
    var urls:[urlModel]?
    var amount:String?
    var currency:String?
    var expenseDate:Timestamp?
    var otherSpecs:String?
    var type:String?
    var admitDate:Timestamp?
    var dischargeDate:Timestamp?
}


struct urlModel:Codable {
    var deviceType:String?
    var type:String?
    var url:String?
    
}
