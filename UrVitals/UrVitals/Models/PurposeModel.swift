//
//  PurposeModel.swift
//  UrVitals
//
//  Created by Sajin M on 17/07/2021.
//

import Foundation

struct PurposeModel:Codable{
    var name:String?
    var imgUrln:String?
}
