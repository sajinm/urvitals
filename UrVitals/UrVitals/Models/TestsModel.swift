//
//  TestsModel.swift
//  UrVitals
//
//  Created by Sajin M on 19/07/2021.
//

import Foundation

struct TestsModel:Codable {
    
    var name:String?
    var imgUrln:String?
    var reportType:[String]?
}
