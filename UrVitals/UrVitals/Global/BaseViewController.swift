//
//  BaseViewController.swift
//  ExpensePRO
//
//  Created by Sajin M on 6/12/19.
//  Copyright © 2019 Codelattice. All rights reserved.
//

import UIKit
import Alamofire
import TextFieldEffects
import CRNotifications
import SVProgressHUD

class BaseViewController: UIViewController {

  
    let progress = SVProgressHUD.self
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
 
    
    fileprivate struct CustomCRNotification: CRNotificationType {
        var textColor: UIColor
        var backgroundColor: UIColor
        var image: UIImage?
    }
    
    
    func isConnected() -> Bool{
        
        
        let customNotification = CustomCRNotification(textColor: UIColor.white, backgroundColor: UIColor.darkGray, image: UIImage(named: "ic_internet"))
           
           if Reachability.isConnectedToNetwork(){
               
             return true
               
           }else{
               
            CRNotifications.showNotification(type: customNotification, title: noInternet, message: noInternet, dismissDelay: 3)
               
               return false
           }
           
           
           
           
       }
    
    func showNotification(message:String){
           
            let customNotification = CustomCRNotification(textColor: UIColor.white, backgroundColor: UIColor.darkGray, image: UIImage(named: "ic_error"))
           
            CRNotifications.showNotification(type: customNotification, title: AppTitle , message: message, dismissDelay: 3)
           
           }
    
    
    class Connectivity {
        class var isConnectedToInternet:Bool {
            return NetworkReachabilityManager()!.isReachable
        }
    }

   
}

extension UIViewController{

    func dismissKey()
    {
    let tap: UITapGestureRecognizer = UITapGestureRecognizer( target: self, action: #selector(UIViewController.dismissKeyboard))
    tap.cancelsTouchesInView = false
    view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard()
    {
    view.endEditing(true)
    }
    
    
    func backNavigation() {
        self.navigationController?.popViewController(animated:true)
    }
    
}
